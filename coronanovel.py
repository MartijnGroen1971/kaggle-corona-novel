#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 17:33:08 2020
 https://www.kaggle.com/sudalairajkumar/novel-corona-virus-2019-dataset

https://www.cia.gov/library/publications/the-world-factbook/docs/faqs.html
https://www.populationpyramid.net/western-europe/2020/

misschien deze groot https://www.cia.gov/library/publications/download/download-2018/index.html
https://www.patreon.com/product/pricing

@author: Martijn Groen
"""
import pandas as pd
import numpy as np
# essential libraries
import json
import random
from urllib.request import urlopen

# storing and anaysis
import numpy as np
import pandas as pd

# visualization
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import plotly.graph_objs as go
import plotly.figure_factory as ff
from scipy import integrate, optimize
# from pandas_profiling import ProfileReport

#constants
ISTEST=0

pad=""
datapad=""

#functions and classes 
class mdf(pd.DataFrame):
    
    # __init__(${1:name}, bases, dict):
    #     df=super().__init__(${1:name}, bases, dict)
    def minfo(self):
        np.set_printoptions(threshold=np.inf)
        pd.set_option("display.max.columns",30)
        print(str(self.info()))
        print(str(self.head(5)))
        print(str(self.isna().sum()))
        print(str(self.describe().T))
       

# This functions smooths data, thanks to Dan Pearson. We will use it to smooth the data for growth factor.
def smoother(inputdata,w,imax):
    data = 1.0*inputdata
    data = data.replace(np.nan,1)
    data = data.replace(np.inf,1)
    #print(data)
    smoothed = 1.0*data
    normalization = 1
    for i in range(-imax,imax+1):
        if i==0:
            continue
        smoothed += (w**abs(i))*data.shift(i,axis=0)
        normalization += w**abs(i)
    smoothed /= normalization
    #if smoothed==-inf 
    return smoothed

def growth_factor(confirmed):
    confirmed_iminus1 = confirmed.shift(1, axis=0)
    confirmed_iminus2 = confirmed.shift(2, axis=0)
    return (confirmed-confirmed_iminus1)/(confirmed_iminus1-confirmed_iminus2)

def growth_ratio(confirmed):
    confirmed_iminus1 = confirmed.shift(1, axis=0)
    return (confirmed/confirmed_iminus1)       

def coronadpng_build():
    # #coronad0.png pie wereld
    # fig1, ax1 = plt.subplots()
    # ax1.pie(tot1, labels=['Deaths', 'Recovered', 'Active'], autopct='%1.1f%%', shadow=True)
    # plt.title("Corona cases in the world (kaggle dataset)")
    # plt.savefig(datapad+"/"+"coronad0.png",bbox_inches='tight' )
    # plt.show()
    # plt.close()
    
    #coronad1.png totals wereld
    # tot1.plot.bar()
    # plt.title("Corona cases in the world (kaggle dataset)")
    # plt.savefig(datapad+"/"+"coronad1.png",bbox_inches='tight' )
    # plt.show()
    # plt.close()
      
    #coronad2.doden per age
    g=sns.FacetGrid(data=df2,col="gender")
    plt.subplots_adjust(top=0.7)
    g.fig.suptitle('Corona deaths and age')
    g.map(sns.distplot, 'age')
    g.add_legend()
    plt.savefig(datapad+"/"+"coronad2.png",bbox_inches='tight' )
    plt.show()
    plt.close()
    
    #doden coronad3.png
    ax = sns.barplot(x='Deaths', y='Country', data=tot3d)
    ax.set_xlabel('Deaths')
    plt.title("Deaths per country top 5")
    plt.savefig(datapad+"/"+"coronad3.png",bbox_inches='tight' )
    plt.show()
    plt.close()
        
    #coronad4.png in China cumulative per week
    plt.plot( 'Week', 'Deaths', data=totw, marker='o', markerfacecolor='blue', markersize=12, color='skyblue', linewidth=4)
    plt.plot( 'Week', 'Confirmed', data=totw, marker='', color='olive', linewidth=2)
    plt.plot( 'Week', 'Recovered', data=totw, marker='', color='olive', linewidth=2, linestyle='dashed', label="Recovered")
    plt.title("Corona in China")
    plt.legend()
    plt.savefig(datapad+"/"+"coronad4.png",bbox_inches='tight' )
    plt.show()
    plt.close()
    
    # totwh=totwh.loc[totwh["Week"]<totwh["Week"].max(),:]
    # multiple line plot
    sns.catplot(x='Week',y='Deaths',data=totwh,kind='point')
    # sns.factorplot(x='Week',y='Recovered',data=totwh)
    # sns.factorplot(x='Week',y='Confirmed',data=totwh)
    # plt.plot( 'Week', 'Deaths', data=totwh,  color='skyblue')
    # plt.plot( 'Week', 'Confirmed', data=totwh, color='olive')
    # plt.plot( 'Week', 'Recovered', data=totwh, marker='', color='olive', linewidth=2, linestyle='dashed', label="Recovered")
    plt.title("Corona deaths in the Netherlands")
    # plt.legend()
    plt.savefig(datapad+"/"+"coronad4h.png",bbox_inches='tight' )
    plt.show()
    plt.close()
    
def coronadpng_build2oudkag():
    #top 10 countries by number of deaths
    rank=1
    for land in list(tot3d["Country"].unique()):
        totwctmp=totwc.loc[totwc["Country"]==land,['Week' ,'Confirmed', 'Deaths', 'Recovered', 'Active']]
        sns.catplot(x='Week',y='Deaths',data=totwctmp,kind='point')
        plt.title(str(rank)+" Corona deaths "+land)
        plt.savefig(datapad+"/"+"coronad4c"+str(rank)+land+".png",bbox_inches='tight' )
        plt.show()
        plt.close()
        rank=rank+1

def coronadpng_build2():
    #top 10 countries by number of deaths
    rank=1
    
    for land in list(dtop10["Country"].unique()):
        totwctmp=df.loc[df["Country"]==land,['Week' , 'CumDeaths']]
        totwctmp=totwctmp[(totwctmp["CumDeaths"]+totwctmp["CumDeaths"])!=0]
        sns.barplot(x='Week',y='CumDeaths',data=totwctmp,estimator=np.mean,ci=None)
        # sns.catplot(x='Week',y='Deaths',data=totwctmp,kind='point',estimator=np.sum)
        #sns.lineplot(x='Week',y='Deaths',data=totwctmp,estimator=np.sum)
        plt.title(str(rank)+" Corona deaths "+land)
        plt.savefig(datapad+"/"+"coronad4c"+str(rank)+land+".png",bbox_inches='tight' )
        plt.show()
        plt.close()
        rank=rank+1

def coronadpng_build3():
    #top 10 countries by number of deaths
    rank=1
    
    for land in list(dtop10["Country"].unique()):
        totwctmp=df.loc[df["Country"]==land,['Week' , 'Deaths']]
        totwctmp=totwctmp[(totwctmp["Deaths"]+totwctmp["Deaths"])!=0]
        sns.barplot(x='Week',y='Deaths',data=totwctmp,estimator=np.sum,ci=None)
        # sns.catplot(x='Week',y='Deaths',data=totwctmp,kind='point',estimator=np.sum)
        #sns.lineplot(x='Week',y='Deaths',data=totwctmp,estimator=np.sum)
        plt.title(str(rank)+" Corona deaths "+land)
        #plt.savefig(datapad+"/"+coronad4c"+str(rank)+land+".png",bbox_inches='tight' )
        plt.show()
        plt.close()
        rank=rank+1

def coronadpng_build4():
    #top 10 countries by number of deaths
    rank=1
    
    for land in list(dtop10[(dtop10["Country"]!="China") &(dtop10["Country"]!="Spain")].Country.unique()):
        totwctmp=dflast5.loc[dflast5["Country"]==land,['Date' , 'Deaths']]
        totwctmp=totwctmp[(totwctmp["Deaths"]+totwctmp["Deaths"])!=0]
        # totwctmp.set_index("Date",inplace=True)
        x_dates = totwctmp['Date'].dt.strftime('%Y-%m-%d').sort_values().unique()
        fig, ax = plt.subplots()
        sns.barplot(x="Date",y='Deaths',data=totwctmp,estimator=np.sum,ci=None)
        # sns.catplot(x='Week',y='Deaths',data=totwctmp,kind='point',estimator=np.sum)
        #sns.lineplot(x='Date',y='Deaths',data=totwctmp,estimator=np.sum)
        # sns.tsplot(x='Date',y='Deaths',data=totwctmp,estimator=np.sum)
        plt.xticks(rotation=45)
        ax.set_xticklabels(labels=x_dates, rotation=45, ha='right')
        #ax.xaxis.set_major_locator(mdates.WeekdayLocator())
        fig.autofmt_xdate()
        plt.title(str(rank)+" Corona deaths "+land)
        plt.savefig(datapad+"/"+"ecdcd"+str(rank)+land+".png",bbox_inches='tight' )
        plt.show()
        plt.close()
        rank=rank+1

def coronadpng_build5():
    #symptoms covid19
    symptoms={'Symptom':['Fever',
        'Dry cough',
        'Fatigue',
        'Sputum production',
        'Shortness of breath',
        'Muscle pain',
        'Sore throat',
        'Headache',
        'Chills',
        'Nausea or vomiting',
        'Nasal congestion',
        'Diarrhoea',
        'Haemoptysis',
        'Conjunctival congestion'],'Percentage':[87.9,67.7,38.1,33.4,18.6,14.8,13.9,13.6,11.4,5.0,4.8,3.7,0.9,0.8]}

    symptoms=pd.DataFrame(data=symptoms,index=range(14))
    symptoms=symptoms.append({"Percentage" : symptoms[symptoms["Percentage"]<=5].Percentage.sum(),"Symptom" : "Rest"},ignore_index=True)
    symptoms=symptoms[symptoms["Percentage"]>5]
    plt.figure(figsize=(15,15))
    plt.title('Symptoms Covid-19 source en.wikipedia.org/wiki/Coronavirus_disease_2019',fontsize=20)    
    # sns.subtitle("Source https://en.wikipedia.org/wiki/Coronavirus_disease_2019")
    plt.xticks(rotation=45)
    sns.barplot(x="Symptom",y="Percentage",data=symptoms,estimator=np.mean,ci=None)
    #plt.legend(symptoms['Symptom'],loc='best')
    plt.savefig(datapad+"/"+"corsymp.png",bbox_inches='tight' )
    plt.show()
    plt.close()  

#sir001 chart country x 
def sir001_build (country,rank):
    #SIR model 
    country_df = pd.DataFrame()
    train=df[df.Country==country].sort_values(by=["Date"])
    population = float(train.Population.max()*1*10**5)
    # country_df['CumCases'] = train.loc[train['Country']=='Netherlands'].CumCases.diff().fillna(0)
    country_df['Cases'] = train.Cases
    country_df = country_df[10:]
    country_df['day_count'] = list(range(1,len(country_df)+1))
    
    ydata = [i for i in country_df.Cases]
    xdata = country_df.day_count
    ydata = np.array(ydata, dtype=float)
    xdata = np.array(xdata, dtype=float)
    if len(ydata)==0:
        return()
    N = population
    inf0 = ydata[0]
    sus0 = N - inf0
    rec0 = 0.0
    
    #y x tijd beta=from S to I gamma from I to R
    def sir_model(y, x, beta, gamma):
        sus = -beta * y[0] * y[1] / N
        rec = gamma * y[1]
        inf = -(sus + rec)
        return sus, inf, rec
    
    def fit_odeint(x, beta, gamma):
        return integrate.odeint(sir_model, (sus0, inf0, rec0), x, args=(beta, gamma))[:,1]
    
    popt, pcov = optimize.curve_fit(fit_odeint, xdata, ydata)
    fitted = fit_odeint(xdata, *popt)
    
    plt.plot(xdata, ydata, 'o')
    plt.plot(xdata, fitted)
    plt.title(str(rank)+" SIR model vs infected cases Corona "+str(train.Country.max()))
    plt.ylabel("People infected")
    plt.xlabel("Days")
    plt.savefig(datapad+"/"+"sir001"+str(rank)+".png",bbox_inches='tight' )
    plt.show()
    plt.close()
    print("Optimal parameters: beta =", popt[0], " and gamma = ", popt[1])
    #end sir001  

#sir001 charts per country top 10 
def coronadpng_build6():
    #top 10 countries by number of deaths
    rank=1
    for land in list(dtop10["Country"].unique()):
        sir001_build(land,rank)
        rank=rank+1
#end coronadpng_build6 sir001 chart per country

#deaths per 100k top 15 countries chart
def coronadpng_build7():
    dfp=df.groupby(by=["Country"])["DeathsPer100000"].max().dropna().sort_values()
    dfp=dfp[dfp!=np.inf]
    dfp=dfp[dfp!=0]
    dfp=round(dfp.sort_values(ascending=False).head(15))
    plt.figure(figsize=(10,10))
    dfp.plot(kind='bar')
    plt.title("Corona deaths per 100.000 inhabitants ")
    plt.ylabel("Deaths X 100.000 people")
    plt.xlabel("Country")
    plt.savefig(datapad+"/"+"d100k.png",bbox_inches='tight' )
    plt.show()
    plt.close()
    #end coronadpng_build7

#basis df build from kaggle, cleaning, transforming
def c19_build():
    c19=pd.read_csv(datapad+"/"+"covid_19_data.csv",parse_dates=['ObservationDate'] )
    c19 = c19.loc[c19["Country/Region"] != "CruiseShip",:]
    c19['Country/Region'] = c19['Country/Region'].replace('Mainland China', 'China')
    c19=c19.rename(columns={"ObservationDate" : "Date","Country/Region" : "Country","Province/State" : "Province"})
    c19[['Province']] = c19[['Province']].fillna('')
    cases = ['Confirmed', 'Deaths', 'Recovered', 'Active']
    c19['Active'] = c19['Confirmed'] - c19['Deaths'] - c19['Recovered']
    c19[cases] = c19[cases].fillna(0)
    
    c19=c19[(( c19["Province"].isin(["Netherlands",""]) & (c19["Country"]=="Netherlands")))|(c19["Country"]!="Netherlands")]
    #Netherlands european part has sometime ''  somtimes Netherlands, just make province Netherlands
    c19.loc[(c19["Country"]=="Netherlands") & (c19["Province"]!="Netherlands"),["Province"]]="Netherlands"
    # c19.to_excel("zkanweg.xlsx")
    c19["Province"]=c19["Country"]#get rid of new use of provinces in germany, italy, sukkels
    #cumulatief per week per country per provincie
    c19["Date"].apply(lambda x: x.date()) 
    c19["Week"]=c19.Date.dt.week
    return(c19)

#basis df build from ecdc, cleaning, transforming
def df_build():
    df=pd.read_excel("https://www.ecdc.europa.eu/sites/default/files/documents/COVID-19-geographic-disbtribution-worldwide.xlsx")
    df=df.drop(columns=["geoId","countryterritoryCode"])
    df=df.rename(columns={
    "dateRep" : "Date",
    "countriesAndTerritories" : "Country",
    "day" : "Day","month" : "Month",
    "year": "Year","cases" : "Cases",
    "deaths": "Deaths","popData2019":"Population"
    
    })
    df=df[df["Population"].isna()==False]
    df["Population"]=df["Population"]/100000 # number of one hundred thousends
    df["Date"]=pd.Series(pd.to_datetime(dict(year=df.Year, month=df.Month, day=df.Day)))
    df["Date"].apply(lambda x: x.date()) 
    df["Week"]=df.Date.dt.week
    df["Population"]=df["Population"].astype("int64")
    df=df[df["Year"]==2020]
    df.sort_values(by=["Country","Date"],axis=0,ascending=True)
    df.set_index(["Country","Date"],inplace=True)
    df["CumDeaths"]=df.sort_values(by=["Country","Date"],ascending=True).groupby(by=["Country","Date"])["Deaths"].sum().groupby(level=0).cumsum()
    df["CumCases"]=df.sort_values(by=["Country","Date"],ascending=True).groupby(by=["Country","Date"])["Cases"].sum().groupby(level=0).cumsum()
    df.reset_index(inplace=True)
    # Growth Factor
    # w = 0.5
    # df['GrowthFactor'] = growth_factor(df['Deaths'])
    # df['GrowthFactor'] = smoother(df['GrowthFactor'],w,5)
    
    # #Plot confirmed[i]/confirmed[i-1], this is called the growth ratio
    # df['GrowthRatio'] = growth_ratio(df['Deaths'])
    # df['GrowthRatio'] = smoother(df['GrowthRatio'],w,5)
    
    # #Plot the growth rate, we will define this as k in the logistic function presented at the beginning of this notebook.
    # df['GrowthRate']=np.gradient(np.log(df['Deaths']))
    # df['GrowthRate'] = smoother(df['GrowthRate'],0.5,3)
    df["MortalityRate"]=(df["CumDeaths"]/df["CumCases"])*100
    df["DeathsPer100000"]=df["CumDeaths"]/df["Population"]
    # df.replace([np.inf, -np.inf], 0)
    df=df[(df["Deaths"]+df["Cases"])!=0]
    return(df)

# lag_list is range of numbers e.g. 1..7,so column_1 ..column_7
def calculate_lag(df, lag_list, column):
    for lag in lag_list:
        column_lag = column + "_" + str(lag)
        df[column_lag] = df.groupby(['Country'])[column].shift(lag, fill_value=0)
    return df

# lag_list is range of numbers e.g. 1..7,so column_1 ..column_7
def calculate_trend(df, lag_list, column):
    for lag in lag_list:
        trend_column_lag = "Trend_" + column + "_" + str(lag)
        df[trend_column_lag] = (df.groupby(['Country'])[column].shift(0, fill_value=0) - 
                                df.groupby(['Country'])[column].shift(lag, fill_value=0))/df.groupby(['Country'])[column].shift(lag, fill_value=0.001)
    return df


    
#main
if __name__=="__main__":
    sns.set(style="darkgrid")
    c19=c19_build() 
    
    c19week = c19.groupby(["Country","Province","Week"])["Confirmed", "Deaths", "Recovered","Active"].max().reset_index()
    c19week["PercentageRecovered"] = c19week["Recovered"]*100/c19week["Confirmed"]
    c19week["PercentageDeaths"] = c19week["Deaths"]*100/c19week["Confirmed"]
    c19week["PercentageActive"] = c19week["Active"]*100/c19week["Confirmed"]
    # c19week[['c', 'd', 'r','a']] = c19week.groupby(["Country","Province","Date"],as_index=False)['Confirmed', 'Deaths', 'Recovered','Active'].shift(1)
    # c19week["nConfirmed"] = c19week["Confirmed"] - c19week["Confirmed"].shift(1)
    # c19week["nDeaths"] = c19week["Deaths"] - c19week["Deaths"].shift(1)
    # c19week["nRecovered"] = c19week["Recovered"] - c19week["Recovered"].shift(1)
    # c19week["nActive"] = c19week["Active"] - c19week["Active"].shift(1)
    m19=mdf(c19week);m19.minfo()
    c19week.to_excel("zkanweg.xlsx")
    
    #cumulatief per land per week doden
    tot2=c19week.groupby(by=["Country","Week"],as_index=False)['Confirmed', 'Deaths', 'Recovered', 'Active'].sum()
    tot2=tot2.drop(["Confirmed"],axis=1)
    
    tot2d=tot2.loc[tot2["Deaths"]!=0,["Country","Deaths"]]
    tot2d=tot2d.sort_values(by="Deaths",ascending=False).head(10).loc[:,["Country","Deaths"]]
   
    #Totaal per land
    tot3=tot2.loc[(tot2["Week"]<(tot2["Week"].max()))] 
    tot3=tot3.groupby(by=["Country"],as_index=False)['Deaths', 'Recovered', 'Active'].max()
    tot3d=tot3.sort_values(by="Deaths",ascending=False).head(10).loc[:,["Country","Deaths"]]
    
    #pie results
    tot1=tot3.loc[:,['Deaths', 'Recovered', 'Active']].sum()
    totw=c19week.loc[c19week["Country"]=="China",["Week",'Confirmed', 'Deaths', 'Recovered', 'Active']]
    totw=totw.groupby(by=["Week"],as_index=False)['Confirmed', 'Deaths', 'Recovered', 'Active'].sum()
    totwh=c19week.loc[c19week["Country"]=="Netherlands",["Week",'Confirmed', 'Deaths', 'Recovered', 'Active']]
    totwh=totwh.groupby(by=["Week"],as_index=False)['Confirmed', 'Deaths', 'Recovered', 'Active'].sum()
    totwc=c19week.loc[:,["Country","Week",'Confirmed', 'Deaths', 'Recovered', 'Active']]
    totwc=totwc.groupby(by=["Country","Week"],as_index=False)['Confirmed', 'Deaths', 'Recovered', 'Active'].sum()
    
    # totw=totw.loc[totw["Week"]<totw["Week"].max(),:]
    #age, only ['China', 'France', 'Japan', 'South Korea', 'Taiwan', 'Hong Kong','Phillipines', 'Iran']
    ll=pd.read_csv(datapad+"/"+"COVID19_line_list_data.csv" )
    df2=ll.loc[ll["death"]!="0",["age","gender","country"]]
    #end kaggle dataset charts
    
    #ecdc dataset charts
    df=df_build();
    #df=df['Year', 'Cases', 'Deaths', 'Country','Date', 'Week']
    df.to_excel("zdf.xlsx")
    m19=mdf(df);m19.minfo()
    #(['Date', 'Day', 'Month', 'Year', 'Cases', 'Deaths', 'Country','Population','Week' ]
    dtop10=df.groupby(by=["Country"],as_index=False)["Deaths"].sum().sort_values(by="Deaths",ascending=False).head(10).loc[:,["Country","Deaths"]]
    dflast5=df[df["Date"].isin ((np.sort(df["Date"].unique())[::-1])[0:5])]
    
    #dataset ecdc date is newer than true
    dflast5["Date"]=dflast5["Date"].apply(pd.DateOffset(-1))
    
    print("Laatste df Date is "+str(df.Date.max()))
    #corona grahpics build
    if ISTEST!=1:
        #coronadpng_build() ages distribution of the deaths
        coronadpng_build2() # 
        #coronadpng_build3()
        coronadpng_build4() #top 10 countries deaths
        coronadpng_build5() #symptoms catplot
        coronadpng_build6() #sir001 chart per country top 10 deaths
        coronadpng_build7() #deaths/100k top 15 countries
      #  coronadpng_build6kanweg() #sir001 chart per country top 10 deaths
    #end ecdc datacharts
    
    
    
    # train = calculate_lag(train.reset_index(), range(1,7), 'CumCases')
    # train = calculate_lag(train, range(1,7), 'CumDeaths')
    # train = calculate_trend(train, range(1,7), 'CumCases')
    # train = calculate_trend(train, range(1,7), 'CumDeaths')
    # train.replace([np.inf, -np.inf], 0, inplace=True)
    # train.fillna(0, inplace=True)

    # fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15,6))
    # # day_count = 38 is March 1st
    # y1 = country_df[(country_df['Country']==country_dict['Netherlands']) & (country_df['day_count']>30) & (country_df['day_count']<=49)][['CumCases']]
    # x1 = range(0, len(y1))
    # ax1.plot(x1, y1, 'bo--')
    # ax1.set_title("Netherlands ConfirmedCases between days 30 and 49")
    # ax1.set_xlabel("Days")
    # ax1.set_ylabel("ConfirmedCases")
    
    # y2 = country_df[(country_df['Country']==country_dict['Netherlands']) & (country_df['day_count']>30) & (country_df['day_count']<=49)][['CumCases']].apply(lambda x: np.log(x))
    # x2 = range(0, len(y2))
    # ax2.plot(x2, y2, 'bo--')
    # ax2.set_title("Netherlands Log ConfirmedCases between days 30 and 49")
    # ax2.set_xlabel("Days")
    # ax2.set_ylabel("Log ConfirmedCases")   
    
    ##
    
#     Features. Select features
# Dates. Filter train data from 2020-03-01 to 2020-03-18
# Log transformation. Apply log transformation to ConfirmedCases and Fatalities
# Infinites. Replace infinites from the logarithm with 0. Given the asymptotic behavior of the logarithm for log(0),this implies that when applying the inverse transformation (exponential) a 1 will be returned instead of a 0. This problem does not impact many countries, but still needs to be tackled sooner or later in order to obtain a clean solution.
# Train/test split. Split into train/valid/test
# Prediction. Linear Regression, training country by country and joining data
# Submit. Submit results in the correct format, and applying exponential to reverse log transformation
    # Filter selected features
    # data = all_data.copy()
    # features = ['Id', 'ForecastId', 'Country', 'Province_State', 'ConfirmedCases', 'Fatalities', 
    #        'Day_num']
    # data = data[features]
    
    # # Apply log transformation to all ConfirmedCases and Fatalities columns, except for trends
    # data[['ConfirmedCases', 'Fatalities']] = data[['ConfirmedCases', 'Fatalities']].astype('float64')
    # data[['ConfirmedCases', 'Fatalities']] = data[['ConfirmedCases', 'Fatalities']].apply(lambda x: np.log1p(x))
    
    # # Replace infinites
    # data.replace([np.inf, -np.inf], 0, inplace=True)

    # # Split data into train/test
    # def split_data(df, train_lim, test_lim):
        
    #     df.loc[df['Day_num']<=train_lim , 'ForecastId'] = -1
    #     df = df[df['Day_num']<=test_lim]
        
    #     # Train set
    #     x_train = df[df.ForecastId == -1].drop(['ConfirmedCases', 'Fatalities'], axis=1)
    #     y_train_1 = df[df.ForecastId == -1]['ConfirmedCases']
    #     y_train_2 = df[df.ForecastId == -1]['Fatalities']
    
    #     # Test set
    #     x_test = df[df.ForecastId != -1].drop(['ConfirmedCases', 'Fatalities'], axis=1)
    
    #     # Clean Id columns and keep ForecastId as index
    #     x_train.drop('Id', inplace=True, errors='ignore', axis=1)
    #     x_train.drop('ForecastId', inplace=True, errors='ignore', axis=1)
    #     x_test.drop('Id', inplace=True, errors='ignore', axis=1)
    #     x_test.drop('ForecastId', inplace=True, errors='ignore', axis=1)
        
    #     return x_train, y_train_1, y_train_2, x_test
    
    
    # # Linear regression model
    # def lin_reg(X_train, Y_train, X_test):
    #     # Create linear regression object
    #     regr = linear_model.LinearRegression()
    
    #     # Train the model using the training sets
    #     regr.fit(X_train, Y_train)
    
    #     # Make predictions using the testing set
    #     y_pred = regr.predict(X_test)
        
    #     return regr, y_pred
    
    
    # # Submission function
    # def get_submission(df, target1, target2):
        
    #     prediction_1 = df[target1]
    #     prediction_2 = df[target2]
    
    #     # Submit predictions
    #     prediction_1 = [int(item) for item in list(map(round, prediction_1))]
    #     prediction_2 = [int(item) for item in list(map(round, prediction_2))]
        
    #     submission = pd.DataFrame({
    #         "ForecastId": df['ForecastId'].astype('int32'), 
    #         "ConfirmedCases": prediction_1, 
    #         "Fatalities": prediction_2
    #     })
#EOF

